<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function player()
    {
        return $this->hasMany(Round::class, 'game_id', 'id');
    }
}
