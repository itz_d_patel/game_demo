<?php

namespace App\Http\Controllers;

use App\Game;
use App\Round;
use App\User;
use Faker\Guesser\Name;
use Faker\Provider\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function startgame(Request $request)
    {
        $total_team = intval($request->players / $request->rounds);
        $extra_players = $request->players % $request->rounds;

        $users = User::limit($request->players)->pluck('id')->toArray();

        $teams = [];
        for ($i=0;$i<$request->rounds;$i++) {
            $random_users = array_rand($users, $total_team);
            $team = [];
            for ($j=0;$j<$total_team;$j++) {
                array_push($team, $users[$random_users[$j]]);
                unset($users[$random_users[$j]]);
            }
            array_push($teams, $team);
        }
        if ($extra_players > 0) {
            $team = [];
            $users = array_values($users);
            for ($k=0;$k<$extra_players;$k++) {
                array_push($team, $users[$k]);
                unset($users[$k]);
            }
            array_push($teams, $team);
        }

        if ($teams != '') {
            $game_name = Str::random(10);
            foreach ($teams as $team) {
                $team_name = Str::random(5);

                $game = new Game();
                $game->uuid = $game_name;
                $game->team_name = $team_name;
                $game->save();

                foreach ($team as $key => $player_id) {
                    $player = new Round();
                    $player->game_id = $game->id;
                    $player->user_id = $player_id;
                    $player->save();
                }
            }
        }
        return redirect()->route('game', ['id' => $game_name]);
    }

    public function game($game_name)
    {
        $teams = Game::where('uuid', $game_name)->with('player')->get();
        $team_names = Game::where('uuid', $game_name)->pluck('id')->toArray();
        $total_players = Round::whereIn('game_id', $team_names)->count();
        return view('game', compact('teams', 'total_players', 'game_name'));
    }

    public function play($game_name)
    {
        $teams = Game::where('uuid', $game_name)->with('player')->get();
        $team_names = Game::where('uuid', $game_name)->pluck('id')->toArray();
        $total_players = Round::whereIn('game_id', $team_names)->count();

        foreach ($teams as $team) {
            $total_team_player = $team->player->pluck('id')->toArray();
            if (count($total_team_player) > 1) {
                $random_users = array_rand($total_team_player, count($total_team_player)-1);
                for ($i=0;$i<count($random_users);$i++) {
                    Round::where('id', $total_team_player[$random_users[$i]])->delete();
                }
            } else {
                $next = Game::where('id', '>', $team->id)->min('id');
                $loser = rand($team->id, $next);
                Game::where('id', $loser)->delete();
            }
        }
        return view('game', compact('teams', 'total_players', 'game_name'));
    }
}
