<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i=0;$i<50;$i++) {
            $user = new \App\User();
            $user->name = $faker->name;
            $user->email = $faker->unique()->safeEmail;
            $user->save();
        }
    }
}
