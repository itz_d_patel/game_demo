@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a href="{{ route('welcome') }}">Back</a> {{ __('Play Your Game') }}</div>

                    <div class="card-body">
                        <table style="border: 1px solid; margin: 5px;">
                            <tr><th class="text-center" colspan="{{ $total_players }}">Rounds</th></tr>
                            @foreach($teams as $team)
                                <tr>
                                    @foreach($team->player as $member)
                                        <th style="border: 1px solid; padding: 20px;">{{$member->user->name}}</th>
                                    @endforeach
                                </tr>
                            @endforeach
                        </table>
                        <a href="{{ route('play', ['game_name' => $game_name]) }}">
                            <button class="btn btn-success">Play</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
