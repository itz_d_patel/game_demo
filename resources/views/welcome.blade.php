@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Play Your Game') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('startgame') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('No. Of Players') }}</label>

                                <div class="col-md-6">
                                    <input id="players" type="number" class="form-control @error('players') is-invalid @enderror" name="players" value="{{ old('players') }}" required autocomplete="players" min="2" autofocus>

                                    @error('players')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rounds" class="col-md-4 col-form-label text-md-right">{{ __('No. Of Rounds') }}</label>

                                <div class="col-md-6">
                                    <input id="rounds" type="number" class="form-control @error('rounds') is-invalid @enderror" name="rounds" value="{{ old('rounds') }}" required autocomplete="current-rounds" min="1">

                                    @error('rounds')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Start') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
