<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/', 'HomeController@welcome')->name('welcome');
Route::post('startgame', 'HomeController@startgame')->name('startgame');
Route::get('home', 'HomeController@index')->name('home');
Route::get('game/{id}', 'HomeController@game')->name('game');
Route::get('play/{game_name}', 'HomeController@play')->name('play');
